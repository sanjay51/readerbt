package model;

public class Teaser {
    public String title;
    public String question;
    public String answer;
    public String hint;

    public Teaser() {
        this.title = "";
        this.question = "";
        this.answer="";
        this.hint="";
    }
    public Teaser(String title, String question, String answer, String hint) {
        this.title = title;
        this.question = question;
        this.answer = answer;
        this.hint = hint;
    }

    public String getTitle() {
        return this.title;
    }

    public String getQuestion() {
        return this.question;
    }

    public String getAnswer() {
        return this.answer;
    }

    public String getHint() {
        return this.hint;
    }

}
