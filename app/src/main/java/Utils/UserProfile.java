package Utils;

import android.content.Context;
import Utils.AppData.Category;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sanjav on 12/6/14.
 */
public class UserProfile {
    private SharedPreferences pref;
    private Category category;
    List<Integer> readList;
    List<Integer> favouriteList;

    private UserProfile(Category category, Context context) {
        this.category = category;
        pref = context.getSharedPreferences("ReaderBT", context.MODE_PRIVATE);
        readList = null;
        favouriteList = null;
    }

    public static UserProfile getProfileByCategory(Category category, Context context) {
        UserProfile profile = new UserProfile(category, context);
        return profile;
    }

    public List<Integer> getReadList() {
        if (readList == null) {
            String strReadList  = getReadString();
            readList = GeneralUtils.splitStringToIntegerList(strReadList);
        }

        return readList;
    }

    public List<Integer> getFavouriteList() {
        if (favouriteList == null) {
            String strFavouriteList = getFavouriteString();
            favouriteList = GeneralUtils.splitStringToIntegerList(strFavouriteList);
        }

        return favouriteList;
    }

    private String getReadString() {
        return pref.getString(category.toString() + "ReadList", null);
    }

    private String getFavouriteString() {
        return pref.getString(category.toString() + "FavouriteList", null);
    }

    public boolean isIndexRead(int index) {
        return (this.getReadList().indexOf(index) != -1 ? true : false);
    }

    public boolean isIndexFavourite(int index) {
        return (this.getFavouriteList().indexOf(index) != -1 ? true : false);
    }

    public void markIndexRead(int index) {
        SharedPreferences.Editor editor = pref.edit();
        String oldValue = getReadString();
        if (oldValue == null || oldValue.isEmpty()) {
            editor.putString(category.toString() + "ReadList", index + "");
        } else {
            editor.putString(category.toString() + "ReadList", oldValue + "," + index);
        }
        editor.commit();
    }

    public void markIndexFavourite(int index) {
        SharedPreferences.Editor editor = pref.edit();
        String oldValue = getFavouriteString();

        if(oldValue == null || oldValue.isEmpty()) {
            editor.putString(category.toString() + "FavouriteList", index + "");
        } else {
            editor.putString(category.toString() + "FavouriteList", oldValue + "," +index);
        }
        editor.commit();
    }

    public void unmarkIndexFavourite(int index) {
        SharedPreferences.Editor editor = pref.edit();
        List<Integer> oldList = getFavouriteList();

        if(oldList.isEmpty()) {
            return;
        }

        List<Integer> newList = new ArrayList<Integer>();
        for(int i = 0; i<oldList.size(); i++) {
            if(oldList.get(i) == index) {
                continue;
            }
            newList.add(oldList.get(i));
        }

        String newFavStr = GeneralUtils.convertIntegerListToString(newList);
        editor.putString(category.toString() + "FavouriteList", newFavStr);
        editor.commit();
    }
}
