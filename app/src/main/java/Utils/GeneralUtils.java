package Utils;

import com.codingg.readerBT.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sanjav on 11/22/14.
 */
public class GeneralUtils {
    public static List<Integer> splitStringToIntegerList(String str) {
        //Delimiter should be comma.
        List<Integer> list = new ArrayList<Integer>();
        if (str == null || str.isEmpty()) return list;
        String[] values = str.split(",");
        for(int i = 0; i<values.length; i++) {
            String value = values[i];
            Integer val = Integer.parseInt(value);
            list.add(val);
        }
        return list;
    }

    public static String convertIntegerListToString(List<Integer> list) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i == list.size() - 1) {
                str.append(list.get(i));
            } else {
                str.append(list.get(i) + ",");
            }
        }
        return str.toString();
    }
}
