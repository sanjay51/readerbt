package Utils;

import com.codingg.readerBT.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sanjav on 11/23/14.
 */
public class AppData {
    static Integer[] HomeGridThumbIds  = {R.drawable.brain_teaser,
            R.drawable.cryptarithms_icon, R.drawable.teaser_icon,
            R.drawable.birbal_the_wise_icon, R.drawable.decipher_icon,
            R.drawable.help_icon, R.drawable.noad_icon};
    static String [] HomeGridTitleList = {"Witty Riddles (103)",
            "Cryptarithms (24)", "Brain Teasers (20)",
            "Birbal - the wise (19)", "Decipher (20)",
            "About Us", "Remove Ads"};

    public static enum Category { Witty_Riddles,
        Cryptarithms, Brain_Teasers, Birbal_The_Wise,
        Decipher, About_Us, Remove_ads };

    static Map<Category, String> CategoryToFileMap = null;
    static {
        CategoryToFileMap = new HashMap<Category, String>();
        CategoryToFileMap.put(Category.Witty_Riddles,   "witty_riddles.xml");
        CategoryToFileMap.put(Category.About_Us,        "about.xml");
        CategoryToFileMap.put(Category.Cryptarithms,    "cryptoarithmatics.xml");
        CategoryToFileMap.put(Category.Brain_Teasers,   "brain_teasers.xml");
        CategoryToFileMap.put(Category.Birbal_The_Wise, "birbal_the_wise.xml");
        CategoryToFileMap.put(Category.Decipher,        "decipher.xml");
    }

    public static String getVolumeTitleByCategory(Category category) {
        return HomeGridTitleList[category.ordinal()];
    }

    public static int getHomeGridElementCount() {
        return HomeGridThumbIds.length;
    }

    public static Integer getHomeGridImageResource(int index) {
        return HomeGridThumbIds[index];
    }

    public static String getHomeGridTitle(int index) {
        return HomeGridTitleList[index];
    }

    public static Category getCategoryByGridPosition(int position) {
        if (position >= Category.values().length)
            return null;

        return Category.values()[position];
    }

    public static String getFileNameByCategory(Category category) {
        return CategoryToFileMap.get(category);
    }

    public static boolean isAdFreeMode() {
        return false;
    }

}
