package Data;

import java.io.InputStream;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;

import android.content.Context;
import android.util.Log;
import android.util.Xml;

import Utils.AppData;
import model.Teaser;
import Utils.AppData.Category;

public class DataManager {
    ArrayList<Teaser> TeaserList = null;
    static DataManager witty_riddles_manager   = null;
    static DataManager cryptarithms_manager    = null;
    static DataManager brain_teasers_manager   = null;
    static DataManager about_us_manager        = null;
    static DataManager birbal_the_wise_manager = null;
    static DataManager decipher_manager        = null;
    static Category category = null;
    Context context;

    public Teaser getTeaser(int index) {
        return TeaserList.get(index);
    }

    public int getTeaserCount() {
        return TeaserList.size();
    }

    public ArrayList<String> getTitleList() {
        ArrayList<String> titleList = new ArrayList<String>();
        int count = 0;
        for (Teaser teaser : TeaserList) {
            String title = teaser.getTitle();
            if(title.equals(""))
                title = (count+1) + ". " + teaser.getQuestion().substring(0, 25) + "...";

            titleList.add(title);
            count++;
        }
        return titleList;
    }

    public static DataManager getManager(Context context, Category category) {
        if(category == Category.Witty_Riddles) {
            if(witty_riddles_manager == null) {
                witty_riddles_manager = new DataManager(context, category);
            }
            return witty_riddles_manager;

        } else if(category == Category.Cryptarithms) {
            if(cryptarithms_manager == null) {
                cryptarithms_manager = new DataManager(context, category);
            }
            return cryptarithms_manager;

        } else if (category == Category.Brain_Teasers) {
            if (brain_teasers_manager == null) {
                brain_teasers_manager = new DataManager(context, category);
            }
            return brain_teasers_manager;

        }  else if (category == Category.About_Us) {
            if (about_us_manager == null) {
                about_us_manager = new DataManager(context, category);
            }
            return about_us_manager;

        }  else if (category == Category.Birbal_The_Wise) {
            if (birbal_the_wise_manager == null) {
                birbal_the_wise_manager = new DataManager(context, category);
            }
            return birbal_the_wise_manager;

        }  else if (category == Category.Decipher) {
            if (decipher_manager == null) {
                decipher_manager = new DataManager(context, category);
            }
            return decipher_manager;
        }

        return null;
    }

    private DataManager(Context context, Category category) {
        TeaserList = new ArrayList<Teaser>();
        this.context = context;
        this.category = category;

        doXMLStuff();

    }

    public void doXMLStuff() {
        String fileName = AppData.getFileNameByCategory(category);

        try {
            XmlPullParser parser = Xml.newPullParser();

            InputStream in_s = context.getAssets().open(fileName);

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in_s, null);

            int eventType = parser.getEventType();
            Teaser teaser = null;

            while (eventType != XmlPullParser.END_DOCUMENT) {
                String name = null;
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        break;
                    case XmlPullParser.START_TAG:
                        name = parser.getName();
                        if (name.equals("title")) {
                            teaser = new Teaser();
                            teaser.title = parser.nextText();
                        } else if (name.equals("question")) {
                            teaser.question = parser.nextText();
                        } else if (name.equals("hint")) {
                            teaser.hint = parser.nextText();
                        } else if (name.equals("answer")) {
                            teaser.answer = parser.nextText();
                            TeaserList.add(teaser);
                        } else if (name.equals("content")) {
                            //About Us case
                            teaser.question = parser.nextText();
                            TeaserList.add(teaser);
                        }

                        break;
                }
                eventType = parser.next();
            }

        } catch (Exception e) {
            Log.e("error", "error" + e.getLocalizedMessage());
        }
    }
}
