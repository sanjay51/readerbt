package com.codingg.readerBT;

import java.util.ArrayList;

import Data.DataManager;
import Utils.AppData;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.InterstitialAd;

import Utils.AppData.Category;
import com.google.android.gms.ads.*;

public class VolumeActivity extends Activity {
    private Category category = null;

    public Activity This = null;
    private InterstitialAd interstitial;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volume);
        category = (Category) getIntent().getExtras().get("category");
        This = this;

        initializeIndex();
        initializeAd();
    }

    public void initializeIndex() {
        final ListView listview = (ListView) findViewById(R.id.listIndex);

        final ArrayList<String> list =  DataManager.getManager(this, category).getTitleList();

        final TextView tvVolumeTitle = (TextView) findViewById(R.id.tvVolumeTitle);
        tvVolumeTitle.setText(AppData.getVolumeTitleByCategory(category));

        final ListViewAdapter adapter = new ListViewAdapter(this, list, category);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                if(category == Category.About_Us) {
                    Intent i = new Intent(This, AboutUsActivity.class);
                    i.putExtra("position", position);
                    i.putExtra("category", category);
                    startActivity(i);
                } else {
                    Intent i = new Intent(This, ReadActivity.class);
                    i.putExtra("position", position);
                    i.putExtra("category", category);
                    startActivity(i);
                }
                initializeAd();
            }

        });
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeIndex();
        displayInterstitial();
    }

    // Invoke displayInterstitial() when you are ready to display an interstitial.
    public void displayInterstitial() {
        if (AppData.isAdFreeMode()) return;

        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }

    public void initializeAd() {
        if (AppData.isAdFreeMode()) {
            return;
        }

        // Create the interstitial.
        interstitial = new InterstitialAd(this);
        interstitial.setAdUnitId("ca-app-pub-3829706966903122/5725756769");

        // Create ad request.
        AdRequest adRequest = new AdRequest.Builder().build();

        // Begin loading your interstitial.
        interstitial.loadAd(adRequest);
    }
}
