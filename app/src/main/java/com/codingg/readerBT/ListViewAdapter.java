package com.codingg.readerBT;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.HashMap;
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import Utils.AppData;
import Utils.AppData.Category;
import Utils.UserProfile;

/**
 * Created by sanjav on 12-Jul-14.
 */

public class ListViewAdapter extends BaseAdapter {
    Context context;
    List<String> titleList;
    Category category;

    public ListViewAdapter(Context a, List<String> itemTitles, Category category) {
        context = a;
        titleList = itemTitles;
        this.category = category;
    }

    public int getCount() {
        return titleList.size();
    }

    public View getItem(int position) {
        return makeItemView(titleList.get(position), position);
    }

    public long getItemId(int position) {
        return position;
    }

    private View makeItemView(String strTitle, int index) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // ViewitemViewR.layout.item
        View itemView = inflater.inflate(R.layout.listview_item, null);

        // findViewById()R.layout.item
        TextView title = (TextView) itemView.findViewById(R.id.text);
        title.setText(strTitle);
        TextView tvIndex = (TextView) itemView.findViewById(R.id.listViewItemIndex);

        UserProfile profile = UserProfile.getProfileByCategory(category, context);
        if(profile.isIndexRead(index)) {
            ImageView ivCheck = (ImageView) itemView.findViewById(R.id.ivCheck);
            ivCheck.setVisibility(View.VISIBLE);
        }

        if (profile.isIndexFavourite(index)) {
            ImageView ivStar = (ImageView) itemView.findViewById(R.id.ivStar);
            ivStar.setVisibility(View.VISIBLE);
        }

        tvIndex.setText(index<9 ? index+1+"  ": index+1+"");

        return itemView;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
            return getItem(position);
    }
}