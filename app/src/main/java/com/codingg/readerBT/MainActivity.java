package com.codingg.readerBT;

import Utils.AppData;
import Utils.UserProfile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class MainActivity extends ActionBarActivity {

    public Activity This = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        This = this;

        initializeIndex();
    }

    public void initializeIndex() {
        final GridView gridView = (GridView) findViewById(R.id.gridIndex);

        final GridArrayAdapter adapter = new GridArrayAdapter();
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {
                if (position == AppData.getHomeGridElementCount() - 1) {
                    //Remove Ads section
                    Intent i = new Intent(This, RemoveAdsActivity.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(This, VolumeActivity.class);
                    i.putExtra("category", AppData.getCategoryByGridPosition(position));
                    startActivity(i);
                }
            }

        });
    }
}
