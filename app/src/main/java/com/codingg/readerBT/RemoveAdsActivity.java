package com.codingg.readerBT;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class RemoveAdsActivity extends ActionBarActivity {
    private Button btnGetAdFreeVersion = null;
    private TextView tvNoAdMessage = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remove_ads);

        tvNoAdMessage = (TextView) findViewById(R.id.tvNoAdMessage);
        tvNoAdMessage.setText("If you like this app, please consider " +
                "buying the ad-free version, and support further development. " +
                "All the money collected, will be spent in developing new " +
                "features, and will help our developers. " +
                "Thank you.");

        btnGetAdFreeVersion = (Button) findViewById(R.id.btnGetAdFreeVersion);

        btnGetAdFreeVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.codingg.readerBT"));
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_remove_ads, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
