package com.codingg.readerBT;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import Data.DataManager;
import Utils.AppData;
import Utils.AppData.Category;
import model.Teaser;

public class AboutUsActivity extends ActionBarActivity {
    private int      currPosition  = 1;
    private Category category      = null;
    private Context  context       = null;

    private TextView tvTitle       = null;
    private TextView tvStory       = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        setContentView(R.layout.activity_read);
        context = this.getApplicationContext();

        //Initialize views
        tvTitle      = (TextView) findViewById(R.id.tvTitle);
        tvStory    = (TextView) findViewById(R.id.tvStory);

        currPosition    = getIntent().getExtras().getInt("position");
        category = (Category) getIntent().getExtras().get("category");

        setContent(currPosition, category);
    }

    private void setContent(int position, Category category) {
        Teaser teaser = DataManager.getManager(context, category).getTeaser(position);

        tvStory.setText(teaser.getQuestion());
        tvTitle.setText(teaser.getTitle());
    }
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_my, container, false);
            return rootView;
        }
    }


    public static class AdFragment extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_ad, container, false);
        }

        @Override
        public void onActivityCreated(Bundle bundle) {
            super.onActivityCreated(bundle);
            if (! AppData.isAdFreeMode()) {
                AdView mAdView = (AdView) getView().findViewById(R.id.adView);
                AdRequest adRequest = new AdRequest.Builder().build();
                mAdView.loadAd(adRequest);
            }
        }
    }
}
