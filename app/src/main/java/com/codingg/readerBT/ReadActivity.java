package com.codingg.readerBT;

import Data.DataManager;
import Utils.AppData;
import Utils.AppData.Category;
import Utils.GeneralUtils;
import Utils.UserProfile;
import model.Teaser;

import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class ReadActivity extends ActionBarActivity {
    private int       currPosition       = 1;
    private Category  category           = null;
    private Context   context            = null;

    private TextView  tvAnswer           = null;
    private TextView  tvQuestion         = null;
    private TextView  tvQuickieHint      = null;
    private TextView  tvQuickieTitle     = null;
    private Button    btnShowAnswer      = null;
    private Button    btnShowNext        = null;
    private Button    btnShowLast        = null;
    private Button    btnShowQuickieHint = null;
    private ImageView ivQuickieStar      = null;

    private LinearLayout llMainContent   = null;
    private Teaser       teaser          = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_quickies);
        context = this.getApplicationContext();

        //Initialize views
        btnShowAnswer      = (Button)    findViewById(R.id.btnShowAnswer);
        btnShowNext        = (Button)    findViewById(R.id.btnShowNext);
        btnShowLast        = (Button)    findViewById(R.id.btnShowLast);
        btnShowQuickieHint = (Button)    findViewById(R.id.btnShowQuickieHint);
        tvAnswer           = (TextView)  findViewById(R.id.tvAnswer);
        tvQuestion         = (TextView)  findViewById(R.id.tvQuestion);
        tvQuickieTitle     = (TextView)  findViewById(R.id.tvQuickieTitle);
        tvQuickieHint      = (TextView)  findViewById(R.id.tvQuickieHint);
        ivQuickieStar      = (ImageView) findViewById(R.id.ivQuickieStar);

        llMainContent      = (LinearLayout) findViewById(R.id.llMainContent);


        currPosition      = getIntent().getExtras().getInt("position");
        category          = (Category) getIntent().getExtras().get("category");

        setTeaser(currPosition, category);
        customize();

        btnShowAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvAnswer.setVisibility(view.VISIBLE);
                UserProfile.getProfileByCategory(category,context).markIndexRead(currPosition);
            }
        });

        btnShowNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int newPosition = currPosition + 1;
                if(newPosition >= DataManager.getManager(context, category).getTeaserCount())
                    return;
                setTeaser(newPosition, category);
                tvAnswer.setVisibility(View.GONE);
                tvQuickieHint.setVisibility(View.GONE);
                currPosition = newPosition;
            }
        });

        btnShowLast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int newPosition = currPosition - 1;
                if(newPosition < 0)
                    return;
                setTeaser(newPosition, category);
                tvAnswer.setVisibility(View.GONE);
                currPosition = newPosition;
            }
        });

        ivQuickieStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserProfile profile = UserProfile.getProfileByCategory(category, context);
                if(profile.isIndexFavourite(currPosition)) {
                    ivQuickieStar.setImageResource(R.drawable.not_star_icon);
                    profile.unmarkIndexFavourite(currPosition);
                } else {
                    profile.markIndexFavourite(currPosition);
                    ivQuickieStar.setImageResource(R.drawable.star_icon);
                }
            }
        });

        btnShowQuickieHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnShowQuickieHint.setVisibility(View.GONE);
                tvQuickieHint.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setQuickieStar() {
        UserProfile profile = UserProfile.getProfileByCategory(category, context);
        if(profile.isIndexFavourite(currPosition)) {
            ivQuickieStar.setImageResource(R.drawable.star_icon);
        } else {
            ivQuickieStar.setImageResource(R.drawable.not_star_icon);
        }
    }

    private void setBtnShowQuickieHint() {
        if (! teaser.getHint().isEmpty()) {
            tvQuickieHint.setText(teaser.getHint());
            btnShowQuickieHint.setVisibility(View.VISIBLE);
        } else {
            btnShowQuickieHint.setVisibility(View.GONE);
        }
    }

    private void setTeaser(int position, Category category) {
        teaser = DataManager.getManager(context, category).getTeaser(position);

        tvQuestion.setText(teaser.getQuestion());
        tvAnswer.setText(teaser.getAnswer());
        tvQuickieTitle.setText(teaser.getTitle());

        setQuickieStar();
        setBtnShowQuickieHint();
    }
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_my, container, false);
            return rootView;
        }
    }

    private void customize() {

        if (AppData.isAdFreeMode()) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) llMainContent.getLayoutParams();
            params.bottomMargin = 15;
            llMainContent.setLayoutParams(params);
        }

        switch (category) {
            case Witty_Riddles:
                tvQuestion.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
                break;
            case Brain_Teasers:
            case Birbal_The_Wise:
            case Decipher:
                tvQuestion.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
                break;
            case Cryptarithms:
                tvQuestion.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
                tvQuestion.setGravity(Gravity.RIGHT);
                tvAnswer.setGravity(Gravity.RIGHT);
                break;
        }
    }


    public static class AdFragment extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_ad, container, false);
        }

        @Override
        public void onActivityCreated(Bundle bundle) {
            super.onActivityCreated(bundle);
            if (! AppData.isAdFreeMode()) {
                AdView mAdView = (AdView) getView().findViewById(R.id.adView);
                AdRequest adRequest = new AdRequest.Builder().build();
                mAdView.loadAd(adRequest);
            }
        }
    }
}
