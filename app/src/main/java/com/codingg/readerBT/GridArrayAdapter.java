package com.codingg.readerBT;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import Utils.AppData;
import Utils.GeneralUtils;

/**
 * Created by sanjav on 12-Jul-14.
 */
public class GridArrayAdapter extends BaseAdapter {

    @Override
    public int getCount() {
        if (AppData.isAdFreeMode()) {
            return AppData.getHomeGridElementCount() - 1;
        }
        return AppData.getHomeGridElementCount();
    }

    @Override
    public Object getItem(int i) {
        return getItem(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.layout_homegrid, parent, false);
        }
        ((ImageView) view.findViewById(R.id.grid_icon_view)).setImageResource(AppData.getHomeGridImageResource(i));
        ((TextView) view.findViewById(R.id.grid_title_view)).setText(AppData.getHomeGridTitle(i));

        return view;
    }
}
